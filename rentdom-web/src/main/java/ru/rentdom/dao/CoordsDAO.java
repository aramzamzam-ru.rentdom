package ru.rentdom.dao;

import java.util.List;

import net.aramzamzam.commons.hibernate.dao.GenericHibernateDAO;

import org.apache.tapestry5.hibernate.HibernateSessionManager;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import ru.rentdom.entities.coords.Coord;
import ru.rentdom.entities.coords.MetroStation;
import ru.rentdom.entities.coords.Town;

/**
 * ДАО для работы с пользователем
 * @author solomenchuk
 *
 */
public class CoordsDAO extends GenericHibernateDAO<Coord, Long>{

	public CoordsDAO(HibernateSessionManager sessionManager) {
		super(sessionManager);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Coord> T findByName(String name, Class<T> clazz)
	{
		return (T) getSession()
					.createCriteria(clazz)
					.add(Restrictions.eq("searchable", name.toLowerCase()))
					.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Coord> List<T> findAll(Class<T> coordClass ,int start, int count)
	{
		return getSession()
		.createCriteria(coordClass)
		.addOrder(Order.desc("order"))
		.addOrder(Order.asc("name"))
		.setMaxResults(count)
		.setFirstResult(start)
		.list();
	}
	
	public int countMetroStations(Town town)
	{
		return (Integer) getSession()
		.createCriteria(MetroStation.class)
		.add(Restrictions.eq("town", town))
		.setProjection(Projections.rowCount() )
		.uniqueResult();
	}
}
