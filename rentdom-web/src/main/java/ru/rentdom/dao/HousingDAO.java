package ru.rentdom.dao;

import java.util.Collection;
import java.util.List;

import net.aramzamzam.commons.hibernate.dao.GenericHibernateDAO;
import net.aramzamzam.commons.hibernate.utils.AssociationExample;

import org.apache.tapestry5.hibernate.HibernateSessionManager;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ru.rentdom.entities.User;
import ru.rentdom.entities.housing.Housing;

/**
 * ДАО для работы с пользователем
 * @author solomenchuk
 *
 */
public class HousingDAO extends GenericHibernateDAO<Housing, Long>{

	public HousingDAO(HibernateSessionManager sessionManager) {
		super(sessionManager);
	}
	
	@SuppressWarnings("unchecked")
	public List<Housing> findAllByOwner(User owner)
	{
		return getSession()
				.createCriteria(Housing.class)
				.add(Restrictions.eq("owner", owner))
				.addOrder(Order.desc("updated"))
				.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Housing> findbySample(Housing housing, Collection<Long> metros, Collection<Long> districts, int start, int count)
	{
		AssociationExample example = AssociationExample.create(housing);
		example.setIncludeAssociations(true);
		Criteria crit = getSession().createCriteria(Housing.class)
		.addOrder(Order.asc("updated"))
		.add(example);
		
		if (metros!=null && metros.size()>0)
		{
			example.excludeProperty("metroStation");
			crit.add(Restrictions.in("metroStation.id", metros));
		}
		if (districts!=null && districts.size()>0)
		{
			example.excludeProperty("district");
			crit.add(Restrictions.in("district.id", districts));
		}
		return crit.setFirstResult(start)
				.setMaxResults(count)
				.list();
	}
}
