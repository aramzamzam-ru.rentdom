package ru.rentdom.utils;

import java.util.ArrayList;
import java.util.List;

public enum HousingType 
{
	flat(1), cottage(2); 

	   private int type; 

	   private static final List<HousingType> types = new ArrayList<HousingType>(); 

	   static { 
	      for (HousingType type : HousingType.values()) 
	         types.add(type); 

	   } 

	   public int value() { 
	      return type; 
	   } 

	   private HousingType(int type) { 
	      this.type = type; 
	   }
}