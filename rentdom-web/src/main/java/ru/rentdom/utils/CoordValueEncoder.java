package ru.rentdom.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.tapestry5.ValueEncoder;

import ru.rentdom.entities.coords.Coord;

public class CoordValueEncoder implements ValueEncoder<Coord> {
	private final Map<String, Coord> values;
	
	public CoordValueEncoder(Iterable<Coord> values)
	{
		this.values=new HashMap<String, Coord>();
		for (Coord coord : values)
			this.values.put(coord.getId().toString(), coord);
	}
	
	@Override
	public String toClient(Coord value) {
		return value.getId().toString();
	}

	@Override
	public Coord toValue(String clientValue) {
		return values.get(clientValue);
	}

}
