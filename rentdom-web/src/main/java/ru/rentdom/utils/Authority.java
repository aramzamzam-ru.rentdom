package ru.rentdom.utils;

import org.springframework.security.GrantedAuthority;

/**
 * роль пользователя
 * @author solomenchuk
 *
 */
public class Authority implements GrantedAuthority{
	private static final long serialVersionUID = -6782523228893435446L;
	
	private String authority;

	public Authority(String authority)
	{
		this.authority = authority;
	}
	
	@Override
	public String getAuthority() {
		return authority;
	}

	@Override
	public int compareTo(Object o) {
		if(o==null)
			return authority==null?0:1;
		return o.equals(authority)?0:1;
	}
	
	@Override
	public String toString() {
		return authority;
	}
	
}
