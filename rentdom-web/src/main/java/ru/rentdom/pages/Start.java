package ru.rentdom.pages;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import net.aramzamzam.commons.components.utils.select.InjectSelectionModel;
import net.aramzamzam.commons.pagesbehavoir.services.HttpStatusCode;

import org.apache.tapestry5.ValueEncoder;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;

import ru.rentdom.entities.coords.Coord;
import ru.rentdom.entities.coords.Town;
import ru.rentdom.entities.housing.Housing;
import ru.rentdom.services.coords.CoordsService;
import ru.rentdom.services.housing.HousingService;
import ru.rentdom.utils.CoordValueEncoder;
import ru.rentdom.utils.HousingType;

/**
 * Start page of application rentdom-web.
 */
public class Start
{
	private enum ContextParameters{
		town, page, coordType, coordIds, housingType, rooms
	}
	public enum CoordType
	{
	    district, metro, any
	}
	
	@Property
	private CoordType coordType;

	@InjectSelectionModel(labelField = "name", idField = "id")
	private List<Town> towns;
	
	@Property
	private Housing sample=new Housing();

	@Property
	private List<?> districtsOrMetros;

	private Set<Long> selectedDistrictsOrMetros=new HashSet<Long>();

	@Property
	private Coord districtOrMetro;
	
	private Boolean hasMetro;
	
	@Property(write=false)
	private int page;
	
	private boolean selectedDistrictsOrMetrosCleared=false;
	
	public boolean getDistrictOrMetroChecked()
	{
		return selectedDistrictsOrMetros.contains(districtOrMetro.getId());
	}

	public void setDistrictOrMetroChecked(boolean value)
	{
		if (!selectedDistrictsOrMetrosCleared)
		{
			selectedDistrictsOrMetros.clear();
			selectedDistrictsOrMetrosCleared=true;
		}
		Long key=districtOrMetro.getId();
		if (value)
			selectedDistrictsOrMetros.add(key);
	}

	@SuppressWarnings("unchecked")
	public ValueEncoder<Coord> getCoordValueEncoder()
	{
		return new CoordValueEncoder((Iterable<Coord>) districtsOrMetros);
	}
	
	@Inject
	private CoordsService coordsService;
	
	@Inject
	private HousingService housingService;
	
	void onActivate(Town town, int page, CoordType coordType, String coordIds, HousingType housingType, Byte rooms) {
		sample.setRooms(rooms);
	}

	
	void onActivate(Town town, int page, CoordType coordType, String coordIds, HousingType housingType) {
		sample.setType(housingType);
	}
	
	Object onActivate(Town town, int page, CoordType coordType, String coordIds) {
		if (coordIds!=null)
		{
			String[]id=coordIds.split("\\.");
			for (String s : id)
				selectedDistrictsOrMetros.add(Long.parseLong(s));
		}
		this.coordType = coordType;
		if (town==null)
			return new HttpStatusCode(HttpServletResponse.SC_NOT_FOUND, "Requested resource was not found");
		if (coordIds==null)
			this.coordType=CoordType.any;

		return null;
	}
	
	void onActivate(Town town, int page) {
		this.page = page;
		sample.setTown(town);
	}


	
	void onActivate()
	{
		if (isHasMetro())
			coordType=CoordType.metro;
		else
			coordType=CoordType.district;
		towns=coordsService.findAllTowns();
		if (sample.getTown()==null)
		{
			if (towns.size()>0)
				sample.setTown(towns.get(0));
		}
		if (isHasMetro())
			districtsOrMetros=coordsService.findAllMetroStations(sample.getTown());
		else
			districtsOrMetros=coordsService.findAllDistricts(sample.getTown());
	}
	
	Object onPassivate()
	{
		Map<ContextParameters, Object> context=new LinkedHashMap<ContextParameters, Object>(5);
		if (sample.getTown()==null)
			return null;
		context.put(ContextParameters.town,sample.getTown());
		context.put(ContextParameters.page, page);
		if (selectedDistrictsOrMetros.size()>0 || sample.getRooms()!=null || sample.getType()!=null)
		{
			context.put(ContextParameters.coordType, coordType);
			switch (this.coordType) {
				case metro:
				case district:
					String ids="";
					for (Long c : selectedDistrictsOrMetros)
							ids+="."+c;

					context.put(ContextParameters.coordIds, ids.isEmpty()?null:ids.substring(1));
					break;
				default:
					if (sample.getRooms()!=null || sample.getType()!=null)
						context.put(ContextParameters.coordIds, null);
			}
		}
		if (sample.getRooms()!=null)
		{
			context.put(ContextParameters.housingType, sample.getType());
			context.put(ContextParameters.rooms, sample.getRooms());
		}
		else if (sample.getType()!=null)
			context.put(ContextParameters.housingType, sample.getType());

		return context.values();
	}
	
	public List<Housing> getHousings()
	{
		Set<Long> districts;
		Set<Long> metros;
		switch (coordType) {
		case district:
			districts=selectedDistrictsOrMetros;
			metros=Collections.emptySet();
			break;
		case metro:
			districts=Collections.emptySet();
			metros=selectedDistrictsOrMetros;
			break;
		default:
			metros=Collections.emptySet();
			districts=Collections.emptySet();
			break;
		}
		return housingService.findBySample(sample, metros, districts, 0, 100);
	}
	
	public boolean isHasMetro()
	{
		if (hasMetro==null)
			hasMetro=coordsService.hasMetro(sample.getTown());
		return hasMetro;
	}
	
	public Object getNullContext()
	{
		return 1;
	}
	
	void onSuccessFromSearchForm()
	{
		//reset page number
		page=0;
	}
	
	void onSuccessFromChooseTown()
	{
		//reset search parameters
		selectedDistrictsOrMetros.clear();
		selectedDistrictsOrMetrosCleared=true;
		page=0;
		sample.setRooms(null);
		sample.setType(null);
		this.coordType=null;
	}
}