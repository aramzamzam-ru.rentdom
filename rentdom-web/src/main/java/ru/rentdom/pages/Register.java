package ru.rentdom.pages;

import org.apache.tapestry5.ValidationException;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.PasswordField;
import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import ru.rentdom.entities.User;
import ru.rentdom.services.user.UserService;
import ru.rentdom.services.user.UserService.UserNameErrors;

public class Register {
	@Inject
	private UserService userService;
	
	@Property
	private User user;

	@Property
	private String password1;

	@Inject
	private Messages messages;
	
	@Component
	private Form registerForm;

	@Component
	private PasswordField password;

	void onActivate()
	{
		user=new User();
	}
	
	@CommitAfter
	void onSuccess()
	{
		userService.createUser(user);
	}
	
	void onValidateFromUserName(String userName) throws ValidationException
	{
	    UserNameErrors err = userService.validateUserName(userName);
	    if (err.equals(UserNameErrors.NO_ERRORS))
	    	return;
		throw new ValidationException(messages.get(err.toString()));
	}
	
	void onValidateFromEmail(String email) throws ValidationException
	{
	    UserNameErrors err = userService.validateEmail(email);
	    if (err.equals(UserNameErrors.NO_ERRORS))
	    	return;
		throw new ValidationException(messages.get(err.toString()));
	}
	
	void onValidateFromPassword1(String password) throws ValidationException
	{
		if (password==null)
			throw new ValidationException(messages.get("empty-password"));
	}

	void onValidateFromPassword(String password) throws ValidationException
	{
		if (password==null)
			throw new ValidationException(messages.get("empty-password"));
	}

	void onValidateForm()
	{
		if (password1!=null && user.getPassword()!=null && !password1.equals(user.getPassword()))
			registerForm.recordError(password, messages.get("passwords-not-mathes"));
	}

}
