package ru.rentdom.pages;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;

import net.aramzamzam.commons.tokens.services.TokenManager;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.ValidationException;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import ru.rentdom.entities.User;
import ru.rentdom.services.user.UserService;

public class Recover {
	
	@SuppressWarnings("unused")
	@Property
	private String username;

	@Property
	private String password;

	@SuppressWarnings("unused")
	@Property
	@Persist(PersistenceConstants.FLASH)
	private URI recoverUrl;
	
	@Property(write=false)
	@Persist(PersistenceConstants.FLASH)
	private Block activeBlock;
	
	@Persist(PersistenceConstants.FLASH)
	private User user;
	
	@Inject
	private UserService userService;
	
	@Inject
	private TokenManager tokenManager;
	
	@Inject
	private Messages messages;
	
	@Inject
	private ComponentResources resources;
	
	@Inject
	private Block step1; 
	
	@Inject
	private Block step2;
	
	@Inject
	private Block step3; 
	
	@Inject
	private Block invalidToken;

	@Inject
	private Block success;
	
	void onActivate(User user)
	{
		activeBlock=tokenManager.isValid()?step3:invalidToken;
		this.user=user;
	}

	
	void onActivate()
	{
		if (activeBlock==null)
			activeBlock=step1;
	}
	
	void onValidateFromUsername(String username) throws ValidationException
	{
		user = userService.findByUserName(username);
		if (user==null)
			throw new ValidationException(messages.get("not_found"));
	}
	
	void onSuccessFromRequestFrom() throws URISyntaxException
	{
		Link link = resources.createPageLink(Recover.class, true, user);
		Calendar expiried=Calendar.getInstance();
		expiried.add(Calendar.HOUR, 48);
		recoverUrl=tokenManager.grant(link, expiried.getTime());
		activeBlock=step2;
	}

	@CommitAfter
	void onSuccessFromChangePasswordForm() throws URISyntaxException
	{
		user.setPassword(password);
		userService.changeUserPassword(user);
		activeBlock=success;
	}

}
