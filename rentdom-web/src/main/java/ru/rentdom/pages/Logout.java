package ru.rentdom.pages;

import nu.localhost.tapestry5.springsecurity.services.LogoutService;

import org.apache.tapestry5.ioc.annotations.Inject;

public class Logout {
	@Inject
	private LogoutService logoutService;
	
	Object onActivate()
	{
		logoutService.logout();
		return Start.class;
	}
}
