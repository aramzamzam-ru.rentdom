package ru.rentdom.pages.admin;

import org.apache.tapestry5.annotations.ApplicationState;
import org.springframework.security.annotation.Secured;

import ru.rentdom.entities.User;
import ru.rentdom.utils.RoleConstants;

@Secured(RoleConstants.USER)
public class AbstractAdminPage {

	@ApplicationState
	private User currentUser;

	public User getCurrentUser() {
		return currentUser;
	}

}
