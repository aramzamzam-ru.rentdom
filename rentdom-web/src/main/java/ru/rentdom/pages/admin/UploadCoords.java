package ru.rentdom.pages.admin;

import java.io.File;
import java.io.IOException;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.upload.services.UploadedFile;

import ru.rentdom.services.coords.CoordsImport;

public class UploadCoords extends AbstractAdminPage{
	@Property
    private UploadedFile file;
	
	@SuppressWarnings("unused")
	@Persist(PersistenceConstants.FLASH)
    @Property(write=false)
    private String message;
	
	@Inject
	private CoordsImport coordsImport;

	@CommitAfter
    public void onSuccess() throws IOException
    {
        File f=File.createTempFile("tmp", ".xls");
		file.write(f);
        coordsImport.process(f);
        f.delete();
        message="upload success";
    }
    
    Object onUploadException(FileUploadException ex)
    {
        message = "Upload exception: " + ex.getMessage();
        return this;
    }
}
