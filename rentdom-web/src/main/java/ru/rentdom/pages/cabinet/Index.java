package ru.rentdom.pages.cabinet;

import java.util.List;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;

import ru.rentdom.entities.housing.Housing;
import ru.rentdom.services.housing.HousingService;


public class Index extends AbstractCabinetPage{

	@Property
	private Housing housing;
	
	@Inject
	private HousingService housingService;
	
	public List<Housing> getHousings()
	{
		return housingService.findAllByOwner(getCurrentUser()); 
	}
	
	public Object[] getHousingContext()
	{
		return new Object[]{housing.getTown(), housing.getType(), housing};
	}
}
