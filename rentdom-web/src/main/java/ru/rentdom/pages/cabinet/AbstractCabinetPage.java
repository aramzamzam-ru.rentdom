package ru.rentdom.pages.cabinet;

import org.apache.tapestry5.annotations.ApplicationState;
import org.springframework.security.annotation.Secured;

import ru.rentdom.entities.User;
import ru.rentdom.utils.RoleConstants;

@Secured(RoleConstants.USER)
public class AbstractCabinetPage {

	@ApplicationState
	private User currentUser;

	public User getCurrentUser() {
		return currentUser;
	}

}
