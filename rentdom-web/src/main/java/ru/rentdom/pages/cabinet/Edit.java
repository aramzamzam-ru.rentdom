package ru.rentdom.pages.cabinet;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.aramzamzam.commons.components.utils.select.InjectSelectionModel;
import net.aramzamzam.commons.pagesbehavoir.services.HttpStatusCode;

import org.apache.commons.lang.ArrayUtils;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.apache.tapestry5.ioc.annotations.Inject;

import ru.rentdom.entities.coords.District;
import ru.rentdom.entities.coords.MetroStation;
import ru.rentdom.entities.coords.Town;
import ru.rentdom.entities.housing.Housing;
import ru.rentdom.services.coords.CoordsService;
import ru.rentdom.services.housing.HousingService;
import ru.rentdom.utils.Authority;
import ru.rentdom.utils.HousingType;
import ru.rentdom.utils.RoleConstants;


public class Edit extends AbstractCabinetPage{
	
	@Property
	@Persist(PersistenceConstants.FLASH)
	private Housing housing;
	
	@Property
	private Block activeBlock;
	
	@Inject
	private Block blockHousingType;
	
	@Inject
	private Block blockEditor;
	
	@Inject
	private CoordsService coordsService;
	
	@SuppressWarnings("unused")
	@InjectSelectionModel(labelField = "name", idField = "id")
	private List<Town> towns;
	
	@SuppressWarnings("unused")
	@InjectSelectionModel(labelField = "name", idField = "id")
	private List<District> districts;
	
	@InjectSelectionModel(labelField = "name", idField = "id")
	private List<MetroStation> metros;

	@Inject
	private HousingService housingService;
	
	void onActivate(Town town, HousingType type)
	{
		if (housing!=null && housing.getId()==null)
		{
			housing.setType(type);
			housing.setTown(town);
			housing.setOwner(getCurrentUser());
			housing.setCreated(new Date());
		}
		housing=housing;
		districts=coordsService.findAllDistricts(town);
		metros=coordsService.findAllMetroStations(town);
		activeBlock=blockEditor;
	}
	
	Object onActivate(Town town, HousingType type, Housing housing)
	{
		if (housing==null 
				|| (!housing.getOwner().equals(getCurrentUser()) 
				&& !ArrayUtils.contains(getCurrentUser().getAuthorities(), new Authority(RoleConstants.ADMIN))))
			return new HttpStatusCode(HttpServletResponse.SC_NOT_FOUND, "Requested resource was not found");
		
		this.housing=housing;
		return null;
	}
	
	void onActivate()
	{
		if (activeBlock==null)
		{
			housing=new Housing();
			towns=coordsService.findAllTowns();
			activeBlock=blockHousingType;
		}
			
	}
	
	Object[] onPassivate()
	{
		if (housing==null || housing.getTown()==null || housing.getType()==null)
			return null;
		if (housing.getId()!=null)
			return new Object[]{housing.getTown(), housing.getType(), housing};
		else
			return new Object[]{housing.getTown(), housing.getType()};
	}
	
	public boolean isHasMetro()
	{
		return metros.size()>0;
	}
	
	@CommitAfter
	Object onSuccessFromFormEditor()
	{
		housing.setUpdated(new Date());
		housingService.save(housing);
		housing=null;
		return Index.class; 
	}
	
}
