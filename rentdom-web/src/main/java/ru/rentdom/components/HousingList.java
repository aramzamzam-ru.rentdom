package ru.rentdom.components;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import ru.rentdom.entities.housing.Housing;
import ru.rentdom.services.coords.CoordsService;

public class HousingList {
	private DecimalFormat priceFormat=null;
	
	@Parameter(required=true, allowNull=false)
	@Property(write=false)
	private List<Housing> housings;
	
	@Property
	private Housing housing;
	
	@Inject
	private CoordsService coordsService;
	
	@Inject
	private Messages messages;
	
	public boolean isEmptyList()
	{
		return housings.size()==0;
	}
	
	public DecimalFormat getPriceFormat()
	{
		if (priceFormat==null)
		{
			DecimalFormatSymbols dfs=new DecimalFormatSymbols();
			dfs.setGroupingSeparator(' ');
			priceFormat = new DecimalFormat("###,###", dfs);
		}
		return priceFormat;
	}
	
	public String getPrice()
	{
		return getPriceFormat().format(housing.getPrice())+" "+housing.getCurrency();
	}
	
	public String getMetroOrDistrict()
	{
		if (coordsService.hasMetro(housing.getTown()))
			return messages.format("metro", housing.getMetroStation().getName());
		else
			return messages.format("distrinc", housing.getDistrict().getName());
	}
}
