package ru.rentdom.components.admin;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;

/**
* @author Michael Courcy
*
* This component aims to provide layout service for a page
*
*/
public class Layout {

        @SuppressWarnings("unused")
		@Parameter(defaultPrefix=BindingConstants.LITERAL)
        @Property(write=false)
        private String title = "admin";
        
}
