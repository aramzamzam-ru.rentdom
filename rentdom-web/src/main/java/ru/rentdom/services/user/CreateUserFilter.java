package ru.rentdom.services.user;

import ru.rentdom.entities.User;

/**
 * Фильтр создания пользователя
 * @author solomenchuk
 *
 */
public interface CreateUserFilter {
	User transform(User user, CreateUserService delegate);
}
