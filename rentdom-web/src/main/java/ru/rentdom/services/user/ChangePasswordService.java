package ru.rentdom.services.user;

import ru.rentdom.entities.User;

/**
 * Сервис смены пароля пользователя
 * @author solomenchuk
 *
 */
public interface ChangePasswordService {
	User transform(User user);
}
