package ru.rentdom.services.user;

import ru.rentdom.entities.User;

/**
 * Фильтр смены пароля
 * @author solomenchuk
 *
 */
public interface ChangePasswordFilter {
	User transform(User user, ChangePasswordService delegate);
}
