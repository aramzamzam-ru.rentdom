package ru.rentdom.services.user;

import ru.rentdom.entities.User;

/**
 * Сервис создания пользователя
 * @author solomenchuk
 *
 */
public interface CreateUserService {
	User transform(User user);
}
