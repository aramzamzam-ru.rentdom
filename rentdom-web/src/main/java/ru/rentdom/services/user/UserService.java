package ru.rentdom.services.user;

import ru.rentdom.dao.UserDAO;
import ru.rentdom.entities.User;

/**
 * Сервис работы с пользователем
 * @author solomenchuk
 *
 */
public class UserService {
	public enum UserNameErrors{DUBLICATE, INVALID_SYMBOLS, NO_ERRORS}
	
	private final ChangePasswordService changePasswordService;
	private final CreateUserService createUserService;
	private final UserDAO userDao;

	public UserService(ChangePasswordService changePasswordService, CreateUserService createUserService, UserDAO userDao)
	{
		this.changePasswordService = changePasswordService;
		this.createUserService = createUserService;
		this.userDao = userDao;
	}
	
	/**
	 * найти пользователя по имени
	 * @param userName
	 * 	имя пользователя
	 * @return
	 */
	public User findByUserName(String userName)
	{
		return userDao.findByUserName(userName);
	}
	
	/**
	 * Создать нового пользователя
	 * @param user
	 * 	пользователь, которого еще нет в БД
	 */
	public void createUser(User user)
	{
		userDao.makePersistent(user);
		createUserService.transform(user);
	}
	
	/**
	 * Сменить пароль пользователя
	 * @param user
	 * 	пользователь, которому необходимо сменить пароль
	 */
	public void changeUserPassword(User user)
	{
		changePasswordService.transform(user);
	}
	
	/**
	 * user name validation
	 * @param userName
	 * 	user name
	 * @return
	 * 	validation result
	 */
	public UserNameErrors validateUserName(String userName)
	{
		if (userDao.findByUserName(userName)!=null)
			return UserNameErrors.DUBLICATE;
		return UserNameErrors.NO_ERRORS;
	}
	
	/**
	 * email validation
	 * @param email
	 * 	email
	 * @return
	 * 	validation result
	 */
	public UserNameErrors validateEmail(String email)
	{
		if (userDao.findByEmail(email)!=null)
			return UserNameErrors.DUBLICATE;
		return UserNameErrors.NO_ERRORS;
	}

}
