package ru.rentdom.services.housing;

import org.apache.tapestry5.ioc.ServiceBinder;

import ru.rentdom.dao.HousingDAO;

public class HousingModule {
	public static void bind(ServiceBinder binder)
    {
		binder.bind(HousingService.class);
		binder.bind(HousingDAO.class);
    }
}
