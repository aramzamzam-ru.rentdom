package ru.rentdom.services.housing;

import java.util.Collection;
import java.util.List;

import ru.rentdom.dao.HousingDAO;
import ru.rentdom.entities.User;
import ru.rentdom.entities.housing.Housing;

public class HousingService {
	
	private final HousingDAO housingDao;

	public HousingService(HousingDAO housingDao)
	{
		this.housingDao = housingDao;
	}
	
	public void save(Housing housing)
	{
		housingDao.makePersistent(housing);
	}
	
	public List<Housing> findAllByOwner(User owner)
	{
		return housingDao.findAllByOwner(owner);
	}

	public List<Housing> findBySample(Housing housing, Collection<Long> metros, Collection<Long> districts, int start, int count)
	{
		return housingDao.findbySample(housing, metros, districts, start, count);
	}

}
