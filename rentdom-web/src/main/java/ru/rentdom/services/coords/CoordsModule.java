package ru.rentdom.services.coords;

import org.apache.tapestry5.ioc.ServiceBinder;

import ru.rentdom.dao.CoordsDAO;

public class CoordsModule {
	public static void bind(ServiceBinder binder)
    {
		binder.bind(CoordsImport.class);
		binder.bind(CoordsDAO.class);
		binder.bind(CoordsService.class);
    }
}
