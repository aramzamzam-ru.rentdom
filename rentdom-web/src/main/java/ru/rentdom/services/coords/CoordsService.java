package ru.rentdom.services.coords;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.rentdom.dao.CoordsDAO;
import ru.rentdom.entities.coords.Coord;
import ru.rentdom.entities.coords.District;
import ru.rentdom.entities.coords.MetroStation;
import ru.rentdom.entities.coords.Town;

public class CoordsService {
	private final Map<Town, Boolean> townMap=new HashMap<Town, Boolean>();
	
	private final CoordsDAO coordsDao;

	public CoordsService(CoordsDAO coordsDao)
	{
		this.coordsDao = coordsDao;
	}
	
	public List<Town> findAllTowns()
	{
		return coordsDao.findAll(Town.class, 0, 999);
	}
	
	public <T extends Coord> T findById(Long id, Class<T> entityClass)
	{
		return coordsDao.findById(id, entityClass);
	}
	
	public List<District> findAllDistricts(Town town)
	{
		District district=new District();
		district.setTown(town);
		return coordsDao.findByExample(district,true);
	}
	
	public List<MetroStation> findAllMetroStations(Town town)
	{
		MetroStation metroStation=new MetroStation();
		metroStation.setTown(town);
		return coordsDao.findByExample(metroStation,true);
	}

	public boolean hasMetro(Town town)
	{
		Boolean res = townMap.get(town);
		if (res==null)
		{
			res=coordsDao.countMetroStations(town)>0;
			townMap.put(town, res);
		}
		return res;
	}
}
