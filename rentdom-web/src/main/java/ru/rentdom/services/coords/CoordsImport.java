package ru.rentdom.services.coords;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;

import ru.rentdom.dao.CoordsDAO;
import ru.rentdom.entities.coords.Coord;
import ru.rentdom.entities.coords.Country;
import ru.rentdom.entities.coords.District;
import ru.rentdom.entities.coords.MetroStation;
import ru.rentdom.entities.coords.Town;
import dk.eobjects.metamodel.DataContext;
import dk.eobjects.metamodel.DataContextFactory;
import dk.eobjects.metamodel.data.DataSet;
import dk.eobjects.metamodel.query.Query;
import dk.eobjects.metamodel.schema.Column;
import dk.eobjects.metamodel.schema.Schema;
import dk.eobjects.metamodel.schema.Table;

public class CoordsImport {
	private final CoordsDAO coordsDao;
	private final Logger logger;

	public CoordsImport(CoordsDAO coordsDAO, Logger logger)
	{
		coordsDao = coordsDAO;
		this.logger = logger;
	}
	
	public void process(File excelFile)
	{
		DataContext dataContext = DataContextFactory.createExcelDataContext(excelFile, false);
		Schema schema = dataContext.getDefaultSchema();
		// A simple way of getting the table object is just to pick the first one (the first sheet in the spreadsheet)
		Table table = schema.getTables()[0];
		Column orderColumn = table.getColumnByName("order");
		Column countryColumn = table.getColumnByName("country");
		Column townColumn = table.getColumnByName("town");
		Column districtColumn = table.getColumnByName("district");
		Column metroColumn = table.getColumnByName("metro");
		Query q = new Query().select(orderColumn, countryColumn, townColumn, districtColumn, metroColumn).from(table);
		DataSet dataSet = dataContext.executeQuery(q);

		Map<String, Country> countries=new HashMap<String, Country>();
		Map<String, Town> towns=new HashMap<String, Town>();
		Map<String, District> districts=new HashMap<String, District>();
		Map<String, MetroStation> metroStations=new HashMap<String, MetroStation>();
		
		Country country=new Country();;
		Town town=new Town();
		while (dataSet.next()) {
			String sOrder=((String) dataSet.getRow().getValue(orderColumn)).trim();
		    String sCountry=((String) dataSet.getRow().getValue(countryColumn)).trim();
		    String sTown=((String) dataSet.getRow().getValue(townColumn)).trim();
		    String sDistrict=((String) dataSet.getRow().getValue(districtColumn)).trim();
		    String sMetro=((String) dataSet.getRow().getValue(metroColumn)).trim();

		    if (!sCountry.isEmpty() && !sCountry.equalsIgnoreCase((country.getName())))
	    	{
	    		country=new Country(sCountry);
	    		country=allocate(getKey(sCountry), country, countries);
	    	}

		    if (country==null)
		    	continue;
		    
	    	if (!sTown.isEmpty() && !sTown.equalsIgnoreCase((town.getName())))
	    	{
	    		town=new Town(sTown, country);
	    		town=allocate(getKey(country.getName(), sTown), town, towns);
	    	}

		    if (town==null)
		    	continue;

		    if (sOrder!="")
		    {
		    	byte order = Byte.parseByte(sOrder);
		    	if (!sCountry.isEmpty() && sTown.isEmpty())
		    		country.setOrder(order);
		    	else
		    		town.setOrder(order);
		    }
		    
		    
		    if (!sDistrict.isEmpty())
		    	allocate(getKey(country.getName(), town.getName(), sDistrict), new District(sDistrict, town), districts);
		    if (!sMetro.isEmpty())
		    	allocate(getKey(country.getName(), town.getName(), sMetro), new MetroStation(sMetro, town), metroStations);
		    
		}
		dataSet.close();
	}
	
	private <T extends Coord> T allocate(String key, T entity, Map<String, T> coll)
	{
		T res = coll.get(key); 
		if (res==null)
		{
			List<T> l = coordsDao.findByExample(entity, true, "name", "order");
			if (l.size()>0)
				res=l.get(0);
			else
			{
				logger.debug("new "+entity.getClass().getSimpleName()+": "+entity.getName());
				coordsDao.makePersistent(entity);
				res=entity;
			}
			coll.put(key, res);
		}
		return res;
	}
	
	private String getKey(String... keys)
	{
		String res="";
		for (String key : keys) {
			res+='.'+key.toLowerCase();
		}
		return res;
	}
}
