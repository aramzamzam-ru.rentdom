package ru.rentdom.services.security;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.security.providers.dao.SaltSource;
import org.springframework.security.providers.encoding.PasswordEncoder;

import ru.rentdom.entities.User;
import ru.rentdom.services.user.ChangePasswordService;
import ru.rentdom.services.user.ChangePasswordFilter;
import ru.rentdom.services.user.CreateUserService;
import ru.rentdom.services.user.CreateUserFilter;

public class EncryptPasswordImpl implements ChangePasswordFilter, CreateUserFilter {
	private PasswordEncoder passwordEncoder;
	private SaltSource salt;

	public EncryptPasswordImpl(@Inject PasswordEncoder passwordEncoder, 
    		@Inject SaltSource salt) {
		this.passwordEncoder = passwordEncoder;
		this.salt = salt;
   }
	@Override
	public User transform(User user, ChangePasswordService delegate) {
		user.setPassword(passwordEncoder.encodePassword(user.getPassword(), salt.getSalt(user)));
        return delegate.transform(user);
	}

	@Override
	public User transform(User user, CreateUserService delegate) {
		user.setPassword(passwordEncoder.encodePassword(user.getPassword(), salt.getSalt(user)));
        return delegate.transform(user);
	}

}
