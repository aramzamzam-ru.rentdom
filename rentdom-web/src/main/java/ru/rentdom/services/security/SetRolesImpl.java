package ru.rentdom.services.security;

import ru.rentdom.entities.User;
import ru.rentdom.services.user.CreateUserFilter;
import ru.rentdom.services.user.CreateUserService;
import ru.rentdom.utils.Authority;

public class SetRolesImpl implements CreateUserFilter{

	@Override
	public User transform(User user, CreateUserService delegate) {
		user.setAuthorities(new Authority[]{new Authority("ROLE_USER")});
		return delegate.transform(user);
	}

}
