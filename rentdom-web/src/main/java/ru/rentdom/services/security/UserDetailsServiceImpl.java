package ru.rentdom.services.security;

import org.springframework.security.userdetails.UserDetails;
import org.springframework.security.userdetails.UserDetailsService;
import org.springframework.security.userdetails.UsernameNotFoundException;

import ru.rentdom.entities.User;
import ru.rentdom.services.user.UserService;

public class UserDetailsServiceImpl implements UserDetailsService {

	private final UserService userService;
	
	public UserDetailsServiceImpl(UserService userService) {
		this.userService = userService;
	}
	
	@Override
	public UserDetails loadUserByUsername(String username){
		
		User user=userService.findByUserName(username);
		if (user == null)
				throw new UsernameNotFoundException("User not found: "+username);
		return user;
	}

}
