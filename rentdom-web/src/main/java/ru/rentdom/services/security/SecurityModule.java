package ru.rentdom.services.security;

import net.aramzamzam.commons.tokens.services.TokenModule;

import org.apache.tapestry5.ioc.Configuration;
import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.OrderedConfiguration;
import org.apache.tapestry5.ioc.ServiceBinder;
import org.apache.tapestry5.ioc.annotations.InjectService;
import org.apache.tapestry5.ioc.annotations.SubModule;
import org.apache.tapestry5.services.AliasContribution;
import org.springframework.security.providers.AuthenticationProvider;
import org.springframework.security.providers.encoding.Md5PasswordEncoder;
import org.springframework.security.providers.encoding.PasswordEncoder;
import org.springframework.security.userdetails.UserDetailsService;

import ru.rentdom.services.user.ChangePasswordFilter;
import ru.rentdom.services.user.CreateUserFilter;

@SubModule({
	nu.localhost.tapestry5.springsecurity.services.SecurityModule.class, 	
	TokenModule.class
})
public class SecurityModule {
	public static void bind(ServiceBinder binder) {
		//провайдер пользователей для acegi
    	binder.bind(UserDetailsService.class, UserDetailsServiceImpl.class);
    	//шифрование пароля
		binder.bind(CreateUserFilter.class, EncryptPasswordImpl.class)
				.withId("EncryptPassword");
		//шифрование пароля
		binder.bind(ChangePasswordFilter.class, EncryptPasswordImpl.class)
				.withId("EncryptChangePassword");
		binder.bind(CreateUserFilter.class, SetRolesImpl.class).withId("SetRoles");
	}

	/**
	 * Шифруем пароль при создании пользователя
	 * @param configuration
	 * @param encryptPassword
	 */
	public static void contributeCreateUserService(
			OrderedConfiguration<CreateUserFilter> configuration,
			@InjectService("EncryptPassword") CreateUserFilter encryptPassword,
			@InjectService("SetRoles") CreateUserFilter setRoles) {
		configuration.add("EncryptPassword", encryptPassword);
		configuration.add("SetRoles", setRoles);
	}
	
	/**
	 * Шифруем пароль при смене пароля пользователем
	 * @param configuration
	 * @param EncryptChangingPassword
	 */
	public static void contributeChangePasswordService(
			OrderedConfiguration<ChangePasswordFilter> configuration,
			@InjectService("EncryptChangePassword") ChangePasswordFilter EncryptChangingPassword) {

		configuration.add("EncryptChangePassword", EncryptChangingPassword);
	}
	
	/**
	 * провайдер пользователей для acegi
	 * @param configuration
	 * @param daoAuthenticationProvider
	 */
	public static void contributeProviderManager(
            OrderedConfiguration<AuthenticationProvider> configuration,
            @InjectService("DaoAuthenticationProvider")
            AuthenticationProvider daoAuthenticationProvider) {
        configuration.add("daoAuthenticationProvider", daoAuthenticationProvider);
    }
	
	/**
	 * дефолтовые натройки приложения
	 * @param configuration
	 */
    public static void contributeApplicationDefaults(
            MappedConfiguration<String, String> configuration)
    {
    	//http://www.localhost.nu/java/tapestry-spring-security/conf.html
    	//урл ошибки логина
    	configuration.add( "spring-security.failure.url", "/login/failed" );
    	//урл логина
        configuration.add("spring-security.loginform.url", "/login");
        //алгоритм шифрования пароля
        configuration.add("spring-security.password.salt", "VOVASTY");
    }
    
    public static void contributeAlias(
    	      Configuration<AliasContribution<PasswordEncoder>> configuration ) {

    	      configuration.add( AliasContribution.create(
    	          PasswordEncoder.class,
    	          new Md5PasswordEncoder() ) );
    }    
}
