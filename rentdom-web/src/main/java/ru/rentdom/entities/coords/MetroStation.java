package ru.rentdom.entities.coords;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import ru.rentdom.entities.housing.Housing;

@Entity(name = "metroStation")
@Table(name = "metroStations")
public class MetroStation extends Coord{
	private static final long serialVersionUID = -4821414637331301222L;
	
	private Town town;

	private Set<Housing> housings;
	
	public MetroStation(){super();}
	
	public MetroStation(String name, Town town) {
		super(name);
		this.town = town;
	}
	
	@ManyToOne(optional=false, fetch=FetchType.LAZY)
	public Town getTown() {
		return town;
	}
	public void setTown(Town town) {
		this.town = town;
	}
	
	@OneToMany(mappedBy="metroStation", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	public Set<Housing> getHousings() {
		return housings;
	}
	public void setHousings(Set<Housing> housings) {
		this.housings = housings;
	}

}
