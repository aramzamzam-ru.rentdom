package ru.rentdom.entities.coords;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity(name = "country")
@Table(name = "countries")
@AttributeOverrides( {
            @AttributeOverride(name="name", column = @Column(unique=true) )
    } )
public class Country extends Coord{
	private static final long serialVersionUID = -2175125848463524261L;
	
	private List<Town> towns;
	
	public Country(){super();}
	
	public Country(String name)
	{
		super(name);
	}
	
	@OneToMany(mappedBy="country", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@OrderBy("name ASC")
	public List<Town> getTowns() {
		return towns;
	}

	public void setTowns(List<Town> towns) {
		this.towns = towns;
	}
}
