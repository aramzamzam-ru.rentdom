package ru.rentdom.entities.coords;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.Index;

import net.aramzamzam.commons.hibernate.entities.BasicEntity;

@MappedSuperclass
public class Coord extends BasicEntity{
	private static final long serialVersionUID = -2687840363714751271L;

	private String name;
	private String searchable;
	private byte order;
	
	public Coord(){}
	public Coord(String name)
	{
		setName(name);
		setSearchable(name.toLowerCase());
	}
	
	@Column(length=100)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="searchable", nullable=false, length=100)
	@Index(name="idx_searchable", columnNames="searchable")
	public String getSearchable() {
		return searchable;
	}

	public void setSearchable(String searchable) {
		this.searchable = searchable;
	}
	
	@Column(name="col_order")
	public byte getOrder() {
		return order;
	}
	public void setOrder(byte order) {
		this.order = order;
	}
	
}
