package ru.rentdom.entities.coords;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import ru.rentdom.entities.housing.Housing;

@Entity(name = "town")
@Table(name = "towns")
public class Town extends Coord{
	private static final long serialVersionUID = -6422128437415734774L;
	
	private Country country;
	private List<MetroStation> metroStations;
	private Set<Housing> housings;

	public Town(){super();}
	
	public Town(String name, Country country) {
		super(name);
		this.country = country;
	}

	@ManyToOne(optional=false, fetch=FetchType.LAZY)
	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@OneToMany(mappedBy="town", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@OrderBy("name ASC")
	public List<MetroStation> getMetroStations() {
		return metroStations;
	}

	public void setMetroStations(List<MetroStation> metroStations) {
		this.metroStations = metroStations;
	}
	
	@OneToMany(mappedBy="town", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	public Set<Housing> getHousings() {
		return housings;
	}
	public void setHousings(Set<Housing> housings) {
		this.housings = housings;
	}
}
