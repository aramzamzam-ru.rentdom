package ru.rentdom.entities.housing;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import net.aramzamzam.commons.hibernate.entities.BasicEntity;
import ru.rentdom.entities.User;
import ru.rentdom.entities.coords.District;
import ru.rentdom.entities.coords.MetroStation;
import ru.rentdom.entities.coords.Town;
import ru.rentdom.utils.HousingType;

@Entity(name="housing")
@Table(name="housings")
public class Housing extends BasicEntity {
	private static final long serialVersionUID = -4821414637331301222L;
	
	private String address;
	private MetroStation metroStation;
	private District district;
	private Town town;
	private User owner;
	private Date created;
	private Date updated;
	private boolean active;
	private HousingType type;
	private Byte rooms;
	private Integer price;
	private String currency;
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@ManyToOne(optional=true)
	public MetroStation getMetroStation() {
		return metroStation;
	}
	public void setMetroStation(MetroStation metroStation) {
		this.metroStation = metroStation;
	}
	
	@ManyToOne(optional=false)
	public District getDistrict() {
		return district;
	}
	public void setDistrict(District district) {
		this.district = district;
	}
	
	@ManyToOne(optional=false)
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	@ManyToOne(optional=false)
	public Town getTown() {
		return town;
	}
	public void setTown(Town town) {
		this.town = town;
	}
	
	@Enumerated(EnumType.ORDINAL)
	public HousingType getType() {
		return type;
	}
	public void setType(HousingType type) {
		this.type = type;
	}
	public Byte getRooms() {
		return rooms;
	}
	public void setRooms(Byte rooms) {
		this.rooms = rooms;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	
	@Column(length=4)
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
