package ru.rentdom;

import org.apache.tapestry5.ioc.Registry;
import org.apache.tapestry5.ioc.RegistryBuilder;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class TestService extends Assert{
	private Registry registry;

	@BeforeClass
	public void beforeClass()
	{
		RegistryBuilder builder = new RegistryBuilder();
		builder.add(TestModule.class);
		registry = builder.build();
		registry.performRegistryStartup();
	}
	
	protected <T> T getService(Class<T> clazz)
	{
		return registry.getService(clazz);
	}

	protected <T> T getService(String serviceId, Class<T> clazz)
	{
		return registry.getService(serviceId, clazz);
	}

	@AfterClass
	public void afterClass()
	{
		//for operations done from this thread 
		registry.cleanupThread();
		//call this to allow services clean shutdown
		registry.shutdown();
	}
}
