package ru.rentdom.services.coords;

import java.io.File;

import org.testng.annotations.Test;

import ru.rentdom.TestService;
import ru.rentdom.services.coords.CoordsImport;

public class CoordsImportTest extends TestService{
	@Test
	public void testProcess()
	{
		CoordsImport im=getService(CoordsImport.class);
		im.process(new File("/Users/vovasty/Documents/SampleData.xls"));
	}
}
