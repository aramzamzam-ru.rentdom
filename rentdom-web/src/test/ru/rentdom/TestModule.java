package ru.rentdom;

import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.hibernate.HibernateModule;
import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.annotations.SubModule;

import ru.rentdom.services.AppModule;

@SubModule({
	AppModule.class,
	HibernateModule.class
})
public class TestModule
{
    public static void contributeApplicationDefaults(
            MappedConfiguration<String, String> configuration)
    {
        configuration.add("tapestry.app-package", "ru.rentdom");
        configuration.add(SymbolConstants.PRODUCTION_MODE, "false");
    }
}
