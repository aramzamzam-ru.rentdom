#!/bin/sh

DOJO_LOCAL_BASE=rentdom-web/src/main/webapp/js
DOJO_REMOTE_BASE=http://svn.dojotoolkit.org/src/tags/release-1.3.0b2/
DOJO_MODULES="dojo dijit"

for m in  $DOJO_MODULES ; do 
if [ ! -d $DOJO_LOCAL_BASE/$m ]
	 then
		svn checkout $DOJO_REMOTE_BASE/$m $DOJO_LOCAL_BASE/$m
	else
		svn update $DOJO_LOCAL_BASE/$m
fi
done

